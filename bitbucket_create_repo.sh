#!/bin/bash

### This Script Creates Repository in BitBucket
##This script is called by Groovy Script in TeamCity.
# $1 => Agent Working Directory
# $2 => Email or UserName
# $3 => Bitbucket Password
# $4 => Repo To Be Created
# $5 => Jira No
# $6 => Force
# $7 => Project Name
##
##
## Author: Birender Sinhg
###
#set -x
echo "hello hi"
username=$(echo -n ${2} |jq -s -R -r @uri)
password=$(echo -n ${3} |jq -s -R -r @uri)

check_access () {

    echo "Verify if user can access BitBucket with given credentials"
    curl -u "${1}":"${2}" -s -I https://bitbucket.org/dashboard/overview | head -1 | grep '200'

    if [[ `echo $?` == 0 ]]
       then
	   echo " ${1} has access on BitBucket, will move forword on next task"
       else
	   echo " ${1} deos not has access on BitBucket, existing ......" 
	   exit 1
    fi 
}

echo "hello"

repo_create () {

repo_stat_not=`curl -u "${1}":"${2}"  -s https://api.bitbucket.org/2.0/repositories/birendersingh04/${3} | jq '.error.message'`
repo_stat_already=`curl -u "${1}":"${2}"  -s https://api.bitbucket.org/2.0/repositories/birendersingh04/${3} | jq '.type'`


 if [[ `echo ${repo_stat_already}` == '"repository"' ]]
       then
           echo " Repo already exists."
	   exit 1
 fi

echo "Creating repository with giver values ......."
curl -X POST -u "${1}":"${2}" -s https://api.bitbucket.org/2.0/repositories/birendersingh04/${3} -d "{\\"scm\\": \\"git\\", \\"is_private\\": \\"true\\", \\"fork_policy\\": \\"no_public_forks\\", \\"project\\": {\\"key\\": \\"${4}\\"}  }"| jq 


echo "Repository has been created successfully."
}




check_access $2 $3
repo_create $2 $3 $4 $7

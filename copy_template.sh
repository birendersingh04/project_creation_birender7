#!/bin/bash

### This Script Creates Repository in BitBucket
##This script is called by Groovy Script in TeamCity.
# $1 => Agent Working Directory
# $2 => Email or UserName
# $3 => Bitbucket Password
# $4 => Repo To Be Created
# $5 => Jira No
# $6 => Force
# $7 => Project Name
##
##
## Author: Birender Singh
###
#set -x

username=$(echo -n ${2} |jq -s -R -r @uri)

password=$(echo -n ${3} |jq -s -R -r @uri)

     cd $1
     initial=`basename $1`
     mkdir ../${initial}_checkoutDirfornewrepotocopyfiles

     for i in `ls | grep -v git.sh `; do cp -rvp $i ../${initial}_checkoutDirfornewrepotocopyfiles ; done
     cd ../${initial}_checkoutDirfornewrepotocopyfiles
        git init
        git checkout -b dev
#        git config --global user.email "$2" change this later
        git config --system user.email "birender.singh@tungsten-network.com"
        git config --system user.name "From Teamcity" 
        git add  -f . 
        git commit -am "${5}, Initial commit"
     git remote add origin https://$username:$password@bitbucket.org/birendersingh04/${4}.git
     git push   -u origin dev

